
import setuptools

package_name = "pywarthog"

setuptools.setup(
    name= package_name,
    version="1.0.0",
    author='Chaoyu Shi',
    author_email='edwardscyqq@gmail.com',
    description="pywarthog",
    packages=['pywarthog'],
    zip_safe=False
)


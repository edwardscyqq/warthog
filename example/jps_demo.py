

import time
import sys

from pywarthog.util_module import experiment, scenario_manager, pqueue_min
from pywarthog.domains_module import gridmap

from pywarthog.heuristics_module import octile_heuristic

from pywarthog.search_module import solution
from pywarthog.search_module import problem_instance

from pywarthog.search_module import expansion_policy, flexible_astar_jps
from pywarthog.jps_module import jps_expansion_policy


def run_experiments(algo, alg_name, scenmgr, verbose, checkopt):

    print("id\talg\texpanded\ttouched\treopen\tsurplus\theapops\tnanos\tpcost\tplen\tmap\n")

    for i in list(range(scenmgr.num_experiments())):

        exp = scenmgr.get_experiment(i)
        startid = exp.starty() * exp.mapwidth() + exp.startx()
        goalid = exp.goaly() * exp.mapwidth() + exp.goalx()

        pi = problem_instance(startid, goalid, verbose)
        sol = solution()

        algo.get_path(pi, sol)

        result = str(i)+"\t"+str(alg_name)+"\t"+str(sol.nodes_expanded_)+"\t"+str(sol.nodes_touched_) + "\t" \
        + str(sol.nodes_reopen_) + "\t" \
        + str(sol.nodes_surplus_) + "\t"    \
        + str(sol.heap_ops_) + "\t" \
        + str(sol.time_elapsed_nano_) + "\t"    \
        + str(sol.sum_of_edge_costs_) + "\t"    \
        + str(len(sol.path_)-1) + "\t" \
        + str(scenmgr.last_file_loaded()) + "\n"

        print(result)


def run_jps(scenmgr, mapname, alg_name):
    map = gridmap(mapname)
    expander = jps_expansion_policy(map)
    heuristic = octile_heuristic(map.width(), map.height())
    open = pqueue_min(1024)

    astar = flexible_astar_jps(heuristic, expander, open)

    checkopt = 0
    verbose = 0

    run_experiments(astar, alg_name, scenmgr, verbose, checkopt)




if __name__ == "__main__":
    # ./debug/bin/warthog --scen ./scenarios/Archipelago.map.scen --alg jps

    sfile = "./scenarios/Archipelago.map.scen"

    mapname = ""

    alg = "jps"

    scenmgr = scenario_manager()

    scenmgr.load_scenario(sfile)

    if mapname == "":
        mapname = scenmgr.get_experiment(0).map()
        print(mapname)


    run_jps(scenmgr, mapname, alg)

    scenmgr.clear()












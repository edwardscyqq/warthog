//
// Created by ed on 11/28/21.
//

#include "search_module.hpp"

#include "octile_heuristic.h"
#include "manhattan_heuristic.h"
#include "cbs_ll_heuristic.h"
#include "zero_heuristic.h"

#include "jpsplus_expansion_policy.h"
#include "jps2plus_expansion_policy.h"
#include "jps2_expansion_policy.h"
#include "jps_expansion_policy.h"
#include "jps4c_expansion_policy.h"
#include "gridmap_expansion_policy.h"
#include "sipp_expansion_policy.h"
#include "cbs_ll_expansion_policy.h"
#include "ll_expansion_policy.h"
#include "vl_gridmap_expansion_policy.h"


#include "pqueue.h"
#include "dummy_listener.h"
#include "jps2_expansion_policy.h"
#include "jps_expansion_policy.h"



#include <vector>


PYBIND11_MODULE(search_module, m) {

    init_solution(m);

    init_problem_instance(m);


    init_expansion_policy(m);
    init_search(m);
    init_depth_first_search(m);

    init_search_node(m);

    init_gridmap_expansion_policy(m);


    std::vector<const char *> class_H_vector;
    std::vector<const char *> class_E_vector;
    std::vector<const char *> class_Q_vector;


    init_flexible_astar<
            warthog::octile_heuristic,
            warthog::jpsplus_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_jpsplus");

    init_flexible_astar<
            warthog::octile_heuristic,
            warthog::jps2plus_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_jps2plus");

    init_flexible_astar<
            warthog::octile_heuristic,
            warthog::jps2_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_jps2");

    init_flexible_astar<
            warthog::octile_heuristic,
            warthog::jps_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_jps");

    init_flexible_astar<
            warthog::manhattan_heuristic,
            warthog::jps4c_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_jps4c");

    init_flexible_astar<
            warthog::octile_heuristic,
            warthog::gridmap_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_astar");

    init_flexible_astar<
            warthog::manhattan_heuristic,
            warthog::gridmap_expansion_policy,
            warthog::pqueue_min,
            warthog::dummy_listener>(m, "flexible_astar_astar4c");

//    init_flexible_astar<
//            warthog::manhattan_heuristic,
//            warthog::sipp_expansion_policy,
//            warthog::pqueue_min,
//            warthog::dummy_listener>(m, "flexible_astar_sipp");

//    init_flexible_astar<
//            warthog::cbs_ll_heuristic,
//            warthog::cbs_ll_expansion_policy,
//            warthog::pqueue_min,
//            warthog::dummy_listener>(m, "flexible_cbs_ll");




}




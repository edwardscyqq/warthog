//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_SEARCH_MODULE_HPP
#define WARTHOG_SEARCH_MODULE_HPP

#include "python.hpp"

#include "flexible_astar.h"
#include "pqueue.h"
#include "dummy_listener.h"
#include "search.h"
#include "octile_heuristic.h"
#include "jpsplus_expansion_policy.h"

void init_search(py::module &m);

void init_expansion_policy(py::module &m);

void init_depth_first_search(py::module &m);

void init_problem_instance(py::module &m);

void init_solution(py::module &m);

void init_search_node(py::module &m);

void init_gridmap_expansion_policy(py::module &m);

template<class H,
        class E,
        class Q = warthog::pqueue_min,
        class L = warthog::dummy_listener >
void init_flexible_astar(py::module &m, const std::string &type_str){
//    typedef warthog::flexible_astar<warthog::octile_heuristic,
//            warthog::jpsplus_expansion_policy,
//            warthog::pqueue_min,
//            warthog::dummy_listener> flexible_astar_t;
//
//    py::class_<flexible_astar_t, warthog::search>(m, "flexible_astar", py::multiple_inheritance())
//            .def(py::init<warthog::octile_heuristic*, warthog::jpsplus_expansion_policy*, warthog::pqueue_min*,  warthog::dummy_listener*>())
//
//            ;

    typedef warthog::flexible_astar<H, E, Q, L> flexible_astar_;

//    std::string pyclass_name = std::string("flexible_astar_") + type_str;
    std::string pyclass_name = type_str;

    py::class_<flexible_astar_, warthog::search>(m, pyclass_name.c_str(), py::multiple_inheritance())
            .def(py::init<H*, E*, Q*>(), "heuristic"_a, "expander"_a, "queue"_a)

            .def("get_pathcost", &flexible_astar_::get_pathcost, "get_pathcost")
            .def("get_path", &flexible_astar_::get_path, "get_path")
            .def("closed_list", &flexible_astar_::closed_list, "closed_list")

            .def("get_generated_node", &flexible_astar_::get_generated_node, "get_generated_node")
            .def("apply_to_closed", &flexible_astar_::apply_to_closed, "apply_to_closed")
            .def("set_cost_cutoff", &flexible_astar_::set_cost_cutoff, "set_cost_cutoff")
            .def("get_cost_cutoff", &flexible_astar_::get_cost_cutoff, "get_cost_cutoff")
            .def("set_max_expansions_cutoff", &flexible_astar_::set_max_expansions_cutoff, "set_max_expansions_cutoff")
            .def("get_max_expansions_cutoff", &flexible_astar_::get_max_expansions_cutoff, "get_max_expansions_cutoff")
            .def("set_listener", &flexible_astar_::set_listener, "set_listener")
            .def("mem", &flexible_astar_::mem, "mem")

            ;
}



#endif //WARTHOG_SEARCH_MODULE_HPP


//
// Created by cyshi on 2021/11/30.
//

#include "search_module.hpp"

#include "problem_instance.h"

void init_problem_instance(py::module &m){

    py::class_<warthog::problem_instance> problem_instance(m, "problem_instance");

    problem_instance.def(py::init<>())
            .def(py::init<warthog::sn_id_t, warthog::sn_id_t, bool>())

            .def(py::init<const warthog::problem_instance&>())

            .def("reset", &warthog::problem_instance::reset, "reset")
            .def("print", &warthog::problem_instance::print, "print")


            ;
}





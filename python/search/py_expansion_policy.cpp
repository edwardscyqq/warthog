//
// Created by cyshi on 2021/11/26.
//

#include "search_module.hpp"

#include "expansion_policy.h"

void init_expansion_policy(py::module &m){

    py::class_<warthog::expansion_policy> expansion_policy(m, "expansion_policy");

//    expansion_policy.def(py::init<size_t>());
    expansion_policy.def("get_nodes_pool_size", &warthog::expansion_policy::get_nodes_pool_size, "get_nodes_pool_size")
            .def("reclaim", &warthog::expansion_policy::reclaim, "reclaim")
            .def("reset", &warthog::expansion_policy::reset, "reset")
//            .def("first", &warthog::expansion_policy::first, "first")
//            .def("n", &warthog::expansion_policy::n, "n")
//            .def("get_successor", &warthog::expansion_policy::get_successor, "get_successor")
//            .def("next", &warthog::expansion_policy::next, "next")
            .def("num_successors", &warthog::expansion_policy::num_successors, "num_successors")
            .def("mem", &warthog::expansion_policy::mem, "mem")


            ;
}


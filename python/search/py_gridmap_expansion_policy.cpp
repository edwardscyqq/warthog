//
// Created by ed on 12/4/21.
//

#include "search_module.hpp"

#include "gridmap_expansion_policy.h"


void init_gridmap_expansion_policy(py::module &m){

    py::class_<warthog::gridmap_expansion_policy, warthog::expansion_policy>(m, "gridmap_expansion_policy", py::multiple_inheritance())

                .def(py::init<warthog::gridmap*, bool>(), "map"_a, "manhattan"_a= false)

            .def("expand", &warthog::gridmap_expansion_policy::expand, "expand")

            .def("get_xy", &warthog::gridmap_expansion_policy::get_xy, "get_xy")

            .def("generate_start_node", &warthog::gridmap_expansion_policy::generate_start_node,
                 py::return_value_policy::reference, "generate_start_node")

            .def("generate_target_node", &warthog::gridmap_expansion_policy::generate_target_node,
                 py::return_value_policy::reference, "generate_target_node")

            .def("mem", &warthog::gridmap_expansion_policy::mem, "mem")

            ;


}










//
// Created by cyshi on 2021/11/30.
//

#include "search_module.hpp"

#include "solution.h"


void init_solution(py::module &m){

    py::class_<warthog::solution> solution(m, "solution");

    solution.def(py::init<>())
            .def(py::init<const warthog::solution&>())

            .def("print", &warthog::solution::print, "print")
            .def("print_metrics", &warthog::solution::print_metrics, "print_metrics")
            .def("print_path", &warthog::solution::print_path, "print_path")
            .def("reset", &warthog::solution::reset, "reset")

            .def_readwrite("sum_of_edge_costs_", &warthog::solution::sum_of_edge_costs_)
            .def_readwrite("time_elapsed_nano_", &warthog::solution::time_elapsed_nano_)
            .def_readwrite("nodes_expanded_", &warthog::solution::nodes_expanded_)
            .def_readwrite("nodes_touched_", &warthog::solution::nodes_touched_)
            .def_readwrite("nodes_surplus_", &warthog::solution::nodes_surplus_)
            .def_readwrite("nodes_reopen_", &warthog::solution::nodes_reopen_)
            .def_readwrite("heap_ops_", &warthog::solution::heap_ops_)
            .def_readwrite("path_", &warthog::solution::path_)


            ;

}




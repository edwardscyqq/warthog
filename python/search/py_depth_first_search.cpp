//
// Created by ed on 11/26/21.
//


#include "search_module.hpp"

#include "search.h"
#include "depth_first_search.h"

#include "octile_heuristic.h"
#include "jps_expansion_policy.h"
#include "pqueue.h"


void init_depth_first_search(py::module &m){

    typedef warthog::depth_first_search<
            warthog::octile_heuristic,
            warthog::jps_expansion_policy,
            warthog::pqueue_min>  depth_first_search_t;

    py::class_<warthog::depth_first_search<
            warthog::octile_heuristic,
            warthog::jps_expansion_policy,
            warthog::pqueue_min>, warthog::search>  depth_first_search(m, "depth_first_search", py::multiple_inheritance());


    depth_first_search.def(py::init<warthog::octile_heuristic*,
                                    warthog::jps_expansion_policy*,
                                    warthog::pqueue_min*>());
    depth_first_search.def("get_pathcost", &warthog::depth_first_search<
            warthog::octile_heuristic,
            warthog::jps_expansion_policy,
            warthog::pqueue_min>::get_pathcost, "get_pathcost")

            .def("get_path", &depth_first_search_t::get_path, "get_path")
            .def("apply_on_generate", &depth_first_search_t::apply_on_generate, "apply_on_generate")
            .def("apply_on_expand", &depth_first_search_t::apply_on_expand, "apply_on_expand")

            .def("set_cost_cutoff", &depth_first_search_t::set_cost_cutoff, "set_cost_cutoff")
            .def("get_cost_cutoff", &depth_first_search_t::get_cost_cutoff, "get_cost_cutoff")
            .def("set_max_expansions_cutoff", &depth_first_search_t::set_max_expansions_cutoff, "set_max_expansions_cutoff")
            .def("get_max_expansions_cutoff", &depth_first_search_t::get_max_expansions_cutoff, "get_max_expansions_cutoff")
            .def("mem", &depth_first_search_t::mem, "mem")

            ;
}





//
// Created by ed on 11/29/21.
//

#include "util_module.hpp"

#include "timer.h"

void init_timer(py::module &m){

    py::class_<warthog::timer> timer(m, "timer");
    timer.def(py::init())
            .def("reset", &warthog::timer::reset, "reset")
            .def("start", &warthog::timer::start, "start")
            .def("stop", &warthog::timer::stop, "stop")

            .def("elapsed_time_nano", &warthog::timer::elapsed_time_nano, "elapsed_time_nano")
            .def("elapsed_time_micro", &warthog::timer::elapsed_time_micro, "elapsed_time_micro")
            .def("elapsed_time_sec", &warthog::timer::elapsed_time_sec, "elapsed_time_sec")
            .def("get_time_nano", &warthog::timer::get_time_nano, "get_time_nano")
            .def("get_time_micro", &warthog::timer::get_time_micro, "get_time_micro")
            .def("get_time_sec", &warthog::timer::get_time_sec, "get_time_sec")

            ;
}





//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_UTIL_MODULE_HPP
#define WARTHOG_UTIL_MODULE_HPP


#include "python.hpp"

#include "pqueue.h"


void init_timer(py::module &m);

void init_scenario_manager(py::module &m);
void init_experiment(py::module &m);

void init_pqueue_min(py::module &m);


template <class Comparator = warthog::cmp_less_search_node,
            class QType = warthog::min_q>
void init_pqueue(py::module &m, const std::string &type_str){

    std::string pyclass_name = type_str; //std::string("flexible_astar_") + type_str;

    py::class_<warthog::pqueue<Comparator, QType>>(m, pyclass_name.c_str())
            .def(py::init<Comparator*, unsigned int>())
            .def(py::init<unsigned int>())

            .def("clear", &warthog::pqueue<Comparator, QType>::clear, "clear")
            .def("decrease_key", &warthog::pqueue<Comparator, QType>::decrease_key, "decrease_key")
            .def("push", &warthog::pqueue<Comparator, QType>::push, "push")
            .def("pop", &warthog::pqueue<Comparator, QType>::pop, "pop")
            .def("contains", &warthog::pqueue<Comparator, QType>::contains, "contains")
            .def("peek", &warthog::pqueue<Comparator, QType>::peek, "peek")
            .def("get_heap_ops", &warthog::pqueue<Comparator, QType>::get_heap_ops, "get_heap_ops")
            .def("size", &warthog::pqueue<Comparator, QType>::size, "size")
            .def("is_minqueue", &warthog::pqueue<Comparator, QType>::is_minqueue, "is_minqueue")
            .def("print", &warthog::pqueue<Comparator, QType>::print, "print")
            .def("mem", &warthog::pqueue<Comparator, QType>::mem, "mem")

            ;
}





#endif //WARTHOG_UTIL_MODULE_HPP

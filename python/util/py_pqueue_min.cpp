//
// Created by ed on 11/26/21.
//

#include "util_module.hpp"

#include "pqueue.h"


void init_pqueue_min(py::module &m){

    typedef warthog::pqueue<warthog::cmp_less_search_node, warthog::min_q>  warthog_pqueue_min;

    py::class_<warthog_pqueue_min>  pqueue_min(m, "pqueue_min");


    pqueue_min.def(py::init<warthog::cmp_less_search_node*, unsigned int>(), "default value of size is 1024");

    pqueue_min.def(py::init<unsigned int>(), "default value of size is 1024");

//    pqueue_min.def("__init__", [](warthog::cmp_less_search_node* cmp, unsigned int size=1024)
//    {
//        new warthog_pqueue_min(cmp, size);
//    });
//
//    pqueue_min.def("__init__", [](unsigned int size=1024)
//    {
//        new warthog_pqueue_min(size);
//    });

    pqueue_min.def("clear", &warthog_pqueue_min::clear, "clear")
            .def("decrease_key", &warthog_pqueue_min::decrease_key, "decrease_key")
            .def("push", &warthog_pqueue_min::push, "push")
            .def("pop", &warthog_pqueue_min::pop, "pop")
            .def("contains", &warthog_pqueue_min::contains, "contains")
            .def("peek", &warthog_pqueue_min::peek, "peek")
            .def("get_heap_ops", &warthog_pqueue_min::get_heap_ops, "get_heap_ops")
            .def("size", &warthog_pqueue_min::size, "size")
            .def("is_minqueue", &warthog_pqueue_min::is_minqueue, "is_minqueue")
            .def("print", &warthog_pqueue_min::print, "print")
            .def("mem", &warthog_pqueue_min::mem, "mem")

                ;



//    .def("__init__", [](platform::guid &g, const std::string &str)
//    {
//        new (&g) platform::guid(stoguid(str));
//    });
//    depth_first_search.def("get_pathcost", &warthog::depth_first_search<
//                    warthog::octile_heuristic,
//                    warthog::jps_expansion_policy,
//                    warthog::pqueue_min>::get_pathcost, "get_pathcost")
//
//            .def("get_path", &depth_first_search_t::get_path, "get_path")
//            .def("apply_on_generate", &depth_first_search_t::apply_on_generate, "apply_on_generate")
//            .def("apply_on_expand", &depth_first_search_t::apply_on_expand, "apply_on_expand")
//
//            .def("set_cost_cutoff", &depth_first_search_t::set_cost_cutoff, "set_cost_cutoff")
//            .def("get_cost_cutoff", &depth_first_search_t::get_cost_cutoff, "get_cost_cutoff")
//            .def("set_max_expansions_cutoff", &depth_first_search_t::set_max_expansions_cutoff, "set_max_expansions_cutoff")
//            .def("get_max_expansions_cutoff", &depth_first_search_t::get_max_expansions_cutoff, "get_max_expansions_cutoff")
//            .def("mem", &depth_first_search_t::mem, "mem")
//
//            ;
}


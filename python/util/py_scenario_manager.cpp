//
// Created by ed on 11/25/21.
//

#include "util_module.hpp"

#include "scenario_manager.h"


void init_scenario_manager(py::module &m){

    py::class_<warthog::scenario_manager> scenario_manager(m, "scenario_manager");
    scenario_manager.def(py::init())
            .def("get_experiment", &warthog::scenario_manager::get_experiment, py::return_value_policy::reference, "get_experiment")
            .def("add_experiment", &warthog::scenario_manager::add_experiment, "add_experiment")
            .def("num_experiments", &warthog::scenario_manager::num_experiments, "num_experiments")
            .def("mem", &warthog::scenario_manager::mem, "mem")
            .def("last_file_loaded", &warthog::scenario_manager::last_file_loaded, "last_file_loaded")

            .def("clear", &warthog::scenario_manager::clear, "clear")
            .def("generate_experiments", &warthog::scenario_manager::generate_experiments, "generate_experiments")
            .def("load_scenario", &warthog::scenario_manager::load_scenario, "load_scenario")
            .def("write_scenario", &warthog::scenario_manager::write_scenario, "write_scenario")
            .def("sort", &warthog::scenario_manager::sort, "sort")

            ;
}






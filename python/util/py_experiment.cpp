//
// Created by cyshi on 2021/11/26.
//

#include "util_module.hpp"

#include "experiment.h"


void init_experiment(py::module &m){
    py::class_<warthog::experiment> experiment(m, "experiment");
    experiment.def(py::init<uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, double, std::string>())
            .def("startx", &warthog::experiment::startx, "startx")

            .def("starty", &warthog::experiment::starty, "starty")
            .def("goalx", &warthog::experiment::goalx, "goalx")
            .def("goaly", &warthog::experiment::goaly, "goaly")
            .def("distance", &warthog::experiment::distance, "distance")
            .def("map", &warthog::experiment::map, "map")
            .def("mapwidth", &warthog::experiment::mapwidth, "mapwidth")
            .def("mapheight", &warthog::experiment::mapheight, "mapheight")
            .def("precision", &warthog::experiment::precision, "precision")
            .def("set_precision", &warthog::experiment::set_precision, "set_precision")
            .def("print", &warthog::experiment::print, "print")

//            .def(py::self == py::self)

            ;
}


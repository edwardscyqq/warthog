//
// Created by ed on 11/25/21.
//

#ifndef WARTHOG_PYTHON_HPP
#define WARTHOG_PYTHON_HPP

#include <pybind11/pybind11.h>

// convenience functions
#include <pybind11/operators.h>

// STL conversions
#include <pybind11/stl.h>

// std::chrono::*
#include <pybind11/chrono.h>

// makes certain STL containers opaque to prevent expensive copies
#include <pybind11/stl_bind.h>

// makes std::function conversions work
#include <pybind11/functional.h>


#include <pybind11/complex.h>
#include <pybind11/numpy.h>
#include <pybind11/cast.h>

#include <iostream>


namespace py = pybind11;

using namespace pybind11::literals;



#endif //WARTHOG_PYTHON_HPP

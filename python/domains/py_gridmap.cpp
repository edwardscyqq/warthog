//
// Created by ed on 11/25/21.
//

#include "domains_module.hpp"

#include "gridmap.h"

void init_gridmap(py::module &m){

    py::class_<warthog::gridmap> gridmap(m, "gridmap");
    gridmap.def(py::init<uint32_t, uint32_t>())
            .def(py::init<const char *>())
            .def("to_padded_id", py::overload_cast<uint32_t>(&warthog::gridmap::to_padded_id), "to_padded_id")
            .def("to_padded_id", py::overload_cast<uint32_t, uint32_t>(&warthog::gridmap::to_padded_id), "to_padded_id")

            .def("to_padded_xy", &warthog::gridmap::to_padded_xy, "to_padded_xy")
            .def("to_unpadded_xy", &warthog::gridmap::to_unpadded_xy, "to_unpadded_xy")
            .def("to_unpadded_id", &warthog::gridmap::to_unpadded_id, "to_unpadded_id")

            .def("get_neighbours", &warthog::gridmap::get_neighbours, "get_neighbours")
            .def("get_neighbours_32bit", &warthog::gridmap::get_neighbours_32bit, "get_neighbours_32bit")
            .def("get_neighbours_upper_32bit", &warthog::gridmap::get_neighbours_upper_32bit, "get_neighbours_upper_32bit")

            .def("get_label", py::overload_cast<uint32_t, unsigned int>(&warthog::gridmap::get_label), "get_label")
            .def("get_label", py::overload_cast<uint32_t>(&warthog::gridmap::get_label), "get_label")

            .def("get_mem_ptr", &warthog::gridmap::get_mem_ptr, "get_mem_ptr")

            .def("set_label", py::overload_cast<uint32_t, unsigned int, bool>(&warthog::gridmap::set_label), "set_label")
            .def("set_label", py::overload_cast<uint32_t, bool>(&warthog::gridmap::set_label), "set_label")

            .def("padded_mapsize", &warthog::gridmap::padded_mapsize, "padded_mapsize")
            .def("height", &warthog::gridmap::height, "height")
            .def("width", &warthog::gridmap::width, "width")
            .def("header_height", &warthog::gridmap::header_height, "header_height")
            .def("header_width", &warthog::gridmap::header_width, "header_width")
            .def("filename", &warthog::gridmap::filename, "filename")

            .def("get_num_traversable_tiles", &warthog::gridmap::get_num_traversable_tiles, "get_num_traversable_tiles")
            .def("invert", &warthog::gridmap::invert, "invert")
            .def("print", &warthog::gridmap::print, "print")
//              .def("printdb", &warthog::gridmap::printdb, "printdb")
            .def("mem", &warthog::gridmap::mem, "mem")


            ;
}





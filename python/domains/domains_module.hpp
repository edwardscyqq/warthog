//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_DOMAINS_MODULE_HPP
#define WARTHOG_DOMAINS_MODULE_HPP




#include "python.hpp"

#include "labelled_gridmap.h"

void init_gridmap(py::module &m);


template<class CELL>
void init_labelled_gridmap(py::module &m, const std::string &type_str){

//    std::string pyclass_name = std::string("labelled_gridmap") + type_str;

    std::string pyclass_name = type_str;

    py::class_<warthog::labelled_gridmap<CELL>>(m, pyclass_name.c_str())
            .def(py::init<unsigned int, unsigned int>())
            .def(py::init<const char*>())

            .def("to_padded_id", py::overload_cast<uint32_t>(&warthog::labelled_gridmap<CELL>::to_padded_id), "to_padded_id")
            .def("to_padded_id", py::overload_cast<uint32_t, uint32_t>(&warthog::labelled_gridmap<CELL>::to_padded_id), "to_padded_id")

            .def("to_unpadded_xy", &warthog::labelled_gridmap<CELL>::to_unpadded_xy, "to_unpadded_xy")

            .def("get_label", &warthog::labelled_gridmap<CELL>::get_label, "get_label")

            .def("set_label", py::overload_cast<uint32_t, unsigned int, CELL>(&warthog::labelled_gridmap<CELL>::set_label), "set_label")
            .def("set_label", py::overload_cast<uint32_t, CELL>(&warthog::labelled_gridmap<CELL>::set_label), "set_label")


            .def("height", &warthog::labelled_gridmap<CELL>::height, "height")
            .def("width", &warthog::labelled_gridmap<CELL>::width, "width")
            .def("header_height", &warthog::labelled_gridmap<CELL>::header_height, "header_height")
            .def("header_width", &warthog::labelled_gridmap<CELL>::header_width, "header_width")
            .def("filename", &warthog::labelled_gridmap<CELL>::filename, "filename")
            .def("mem", &warthog::labelled_gridmap<CELL>::mem, "mem")


            ;
}





#endif //WARTHOG_DOMAINS_MODULE_HPP

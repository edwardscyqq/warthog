//
// Created by ed on 11/28/21.
//


#include "domains_module.hpp"

#include "constants.h"

PYBIND11_MODULE(domains_module, m) {

    init_gridmap(m);

    init_labelled_gridmap<warthog::dbword>(m, "vl_gridmap");

}



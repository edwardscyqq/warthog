//
// Created by ed on 12/3/21.
//

#include "mapf_module.hpp"

#include "cbs_ll_expansion_policy.h"
#include "cbs_ll_heuristic.h"
#include "search_node.h"


void init_cbs_ll_expansion_policy(py::module &m){

    py::class_<warthog::cbs_ll_expansion_policy>(m, "cbs_ll_expansion_policy")

            .def(py::init<warthog::gridmap*, warthog::cbs_ll_heuristic*>())

            .def("reset", &warthog::cbs_ll_expansion_policy::reset, "reset")

//            .def("first", &warthog::cbs_ll_expansion_policy::first, "first")

//            .def("n", &warthog::cbs_ll_expansion_policy::n, "n")

//            .def("next", &warthog::cbs_ll_expansion_policy::next, "next")

            .def("expand", &warthog::cbs_ll_expansion_policy::expand, "expand")

            .def("get_xy", &warthog::cbs_ll_expansion_policy::get_xy, "get_xy")


            .def("generate_start_node", &warthog::cbs_ll_expansion_policy::generate_start_node,
                 py::return_value_policy::reference, "generate_start_node")

            .def("generate_target_node", &warthog::cbs_ll_expansion_policy::generate_target_node,
                 py::return_value_policy::reference, "generate_target_node")

            .def("generate", &warthog::cbs_ll_expansion_policy::generate,
                 py::return_value_policy::reference, "generate")

            .def("is_target", &warthog::cbs_ll_expansion_policy::is_target, "is_target")

            .def("add_constraint", &warthog::cbs_ll_expansion_policy::add_constraint, "add_constraint")

            .def("get_constraint", &warthog::cbs_ll_expansion_policy::get_constraint,
                 py::return_value_policy::reference, "get_constraint")

            .def("get_time_constraints", &warthog::cbs_ll_expansion_policy::get_time_constraints,
                 py::return_value_policy::reference, "get_time_constraints")

            .def("mem", &warthog::cbs_ll_expansion_policy::mem, "mem")

            ;
}



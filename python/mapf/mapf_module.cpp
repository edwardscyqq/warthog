//
// Created by ed on 11/28/21.
//

#include "mapf_module.hpp"


PYBIND11_MODULE(mapf_module, m) {

    init_cbs_ll_expansion_policy(m);
    init_cbs_ll_heuristic(m);

}





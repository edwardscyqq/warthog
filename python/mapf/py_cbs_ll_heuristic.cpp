//
// Created by ed on 11/26/21.
//

#include "mapf_module.hpp"

#include "cbs_ll_heuristic.h"
#include "gridmap.h"

void init_cbs_ll_heuristic(py::module &m){

    py::class_<warthog::cbs_ll_heuristic> cbs_ll_heuristic(m, "cbs_ll_heuristic");

    cbs_ll_heuristic.def(py::init<warthog::gridmap*>())
                    .def("h", &warthog::cbs_ll_heuristic::h, "h")
                    .def("set_current_target", &warthog::cbs_ll_heuristic::set_current_target, "set_current_target")
                    .def("mem", &warthog::cbs_ll_heuristic::mem, "mem")

                    ;
}







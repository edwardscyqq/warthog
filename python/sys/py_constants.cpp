//
// Created by ed on 11/26/21.
//

#include "sys_module.hpp"

#include "constants.h"

void init_constants(py::module &m){

    m.attr("SN_ID_MAX") = warthog::SN_ID_MAX;

    m.attr("NO_PARENT") = warthog::NO_PARENT;

    m.attr("DBWORD_BITS") = warthog::DBWORD_BITS;
    m.attr("DBWORD_BITS_MASK") = warthog::DBWORD_BITS_MASK;
    m.attr("LOG2_DBWORD_BITS") = warthog::LOG2_DBWORD_BITS;
    m.attr("DBL_ONE") = warthog::DBL_ONE;
    m.attr("DBL_TWO") = warthog::DBL_TWO;
    m.attr("DBL_ROOT_TWO") = warthog::DBL_ROOT_TWO;
    m.attr("DBL_ONE_OVER_TWO") = warthog::DBL_ONE_OVER_TWO;
    m.attr("DBL_ONE_OVER_ROOT_TWO") = warthog::DBL_ONE_OVER_ROOT_TWO;
    m.attr("DBL_ROOT_TWO_OVER_FOUR") = warthog::DBL_ROOT_TWO_OVER_FOUR;
    m.attr("ONE") = warthog::ONE;

    m.attr("INF32") = warthog::INF32;
    m.attr("INFTY") = warthog::INFTY;
    m.attr("COST_MAX") = warthog::COST_MAX;
    m.attr("COST_MIN") = warthog::COST_MIN;

    m.attr("FNV32_offset_basis") = warthog::FNV32_offset_basis;
    m.attr("FNV32_prime") = warthog::FNV32_prime;

}












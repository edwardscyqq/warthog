//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_SYS_MODULE_HPP
#define WARTHOG_SYS_MODULE_HPP

#include "python.hpp"

void init_constants(py::module &m);


#endif //WARTHOG_SYS_MODULE_HPP

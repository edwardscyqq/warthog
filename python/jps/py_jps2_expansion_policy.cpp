//
// Created by ed on 12/3/21.
//

#include "jps_module.hpp"

#include "jps2_expansion_policy.h"

void init_jps2_expansion_policy(py::module &m){

    py::class_<warthog::jps2_expansion_policy, warthog::expansion_policy>(m, "jps2_expansion_policy", py::multiple_inheritance())

            .def(py::init<warthog::gridmap*>())

            .def("expand", &warthog::jps2_expansion_policy::expand, "expand")

            .def("get_xy", &warthog::jps2_expansion_policy::get_xy, "get_xy")

            .def("generate_start_node", &warthog::jps2_expansion_policy::generate_start_node,
                 py::return_value_policy::reference, "generate_start_node")

            .def("generate_target_node", &warthog::jps2_expansion_policy::generate_target_node,
                 py::return_value_policy::reference, "generate_target_node")

            .def("mem", &warthog::jps2_expansion_policy::mem, "mem")

            ;
}




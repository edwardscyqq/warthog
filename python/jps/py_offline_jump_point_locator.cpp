//
// Created by ed on 12/3/21.
//

#include "jps_module.hpp"

#include "offline_jump_point_locator.h"
#include "gridmap.h"


void init_offline_jump_point_locator(py::module &m){

    py::class_<warthog::offline_jump_point_locator>(m, "offline_jump_point_locator")

            .def(py::init<warthog::gridmap*>())

            .def("jump", &warthog::offline_jump_point_locator::jump, "jump")

            .def("mem", &warthog::offline_jump_point_locator::mem, "mem")

            ;

}





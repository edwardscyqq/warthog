//
// Created by ed on 12/3/21.
//

#include "jps_module.hpp"

#include "online_jump_point_locator.h"


void init_online_jump_point_locator(py::module &m){

    py::class_<warthog::online_jump_point_locator>(m, "online_jump_point_locator")

            .def(py::init<warthog::gridmap*>())

            .def("jump", &warthog::online_jump_point_locator::jump, "jump")

            .def("mem", &warthog::online_jump_point_locator::mem, "mem")

            ;
}


//
// Created by ed on 12/3/21.
//

#include "jps_module.hpp"

#include "online_jump_point_locator2.h"


void init_online_jump_point_locator2(py::module &m){

    py::class_<warthog::jps::online_jump_point_locator2>(m, "online_jump_point_locator2")

            .def(py::init<warthog::gridmap*>())

            .def("jump", &warthog::jps::online_jump_point_locator2::jump, "jump")
            .def("rjump", &warthog::jps::online_jump_point_locator2::rjump, "rjump")

            .def("mem", &warthog::jps::online_jump_point_locator2::mem, "mem")

            ;
}










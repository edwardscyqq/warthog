//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_JPS_MODULE_HPP
#define WARTHOG_JPS_MODULE_HPP

#include "python.hpp"

void init_jps_expansion_policy(py::module &m);
void init_jps2_expansion_policy(py::module &m);
void init_jps2plus_expansion_policy(py::module &m);
void init_jps4c_expansion_policy(py::module &m);
void init_jpsplus_expansion_policy(py::module &m);


void init_offline_jump_point_locator(py::module &m);
void init_offline_jump_point_locator2(py::module &m);
void init_online_jump_point_locator(py::module &m);
void init_online_jump_point_locator2(py::module &m);








#endif //WARTHOG_JPS_MODULE_HPP

//
// Created by ed on 11/28/21.
//

#include "jps_module.hpp"

PYBIND11_MODULE(jps_module, m) {

    init_jps_expansion_policy(m);
    init_jps2_expansion_policy(m);
    init_jps2plus_expansion_policy(m);
    init_jps4c_expansion_policy(m);
    init_jpsplus_expansion_policy(m);

    init_offline_jump_point_locator(m);
    init_offline_jump_point_locator2(m);
    init_online_jump_point_locator(m);
    init_online_jump_point_locator2(m);

}
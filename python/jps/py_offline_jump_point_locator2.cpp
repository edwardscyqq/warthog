//
// Created by ed on 12/3/21.
//

#include "jps_module.hpp"

#include "gridmap.h"
#include "offline_jump_point_locator2.h"


void init_offline_jump_point_locator2(py::module &m){

    py::class_<warthog::offline_jump_point_locator2>(m, "offline_jump_point_locator2")

            .def(py::init<warthog::gridmap*>())

            .def("jump", &warthog::offline_jump_point_locator2::jump, "jump")

            .def("mem", &warthog::offline_jump_point_locator2::mem, "mem")

            ;
}




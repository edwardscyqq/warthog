//
// Created by cyshi on 2021/11/29.
//

#ifndef WARTHOG_HEURISTICS_MODULE_HPP
#define WARTHOG_HEURISTICS_MODULE_HPP


#include "python.hpp"


void init_manhattan_heuristic(py::module &m);
void init_octile_heuristic(py::module &m);



#endif //WARTHOG_HEURISTICS_MODULE_HPP

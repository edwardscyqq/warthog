//
// Created by ed on 11/28/21.
//

#include "heuristics_module.hpp"


PYBIND11_MODULE(heuristics_module, m) {

    init_manhattan_heuristic(m);
    init_octile_heuristic(m);

}




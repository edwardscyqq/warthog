//
// Created by ed on 12/4/21.
//

#include "heuristics_module.hpp"

#include "manhattan_heuristic.h"


void init_manhattan_heuristic(py::module &m){

    py::class_<warthog::manhattan_heuristic>(m, "manhattan_heuristic")

            .def(py::init<uint32_t, uint32_t>())

            .def("h", py::overload_cast<int32_t, int32_t, int32_t, int32_t>(&warthog::manhattan_heuristic::h), "h")
            .def("h", py::overload_cast<warthog::sn_id_t, warthog::sn_id_t>(&warthog::manhattan_heuristic::h), "h")
            .def("mem", &warthog::manhattan_heuristic::mem, "mem")

            ;

}


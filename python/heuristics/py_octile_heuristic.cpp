//
// Created by cyshi on 2021/11/26.
//

#include "heuristics_module.hpp"

#include "octile_heuristic.h"
#include "constants.h"

void init_octile_heuristic(py::module &m){

    py::class_<warthog::octile_heuristic> octile_heuristic(m, "octile_heuristic");

    octile_heuristic.def(py::init<uint32_t, uint32_t>())

            .def("h", py::overload_cast<int32_t, int32_t, int32_t, int32_t>(&warthog::octile_heuristic::h), "h")
            .def("h", py::overload_cast<warthog::sn_id_t, warthog::sn_id_t>(&warthog::octile_heuristic::h), "h")

            .def("set_hscale", &warthog::octile_heuristic::set_hscale, "set_hscale")
            .def("get_hscale", &warthog::octile_heuristic::get_hscale, "get_hscale")
            .def("mem", &warthog::octile_heuristic::mem, "mem")

            ;

}




//
// Created by cyshi on 2021/11/25.
//

#include <pybind11/pybind11.h>

#include <iostream>
#include <vector>
#include <string>

#include "cbs.h"
#include "cbs_ll_expansion_policy.h"
#include "cbs_ll_heuristic.h"
#include "cfg.h"
#include "constants.h"
#include "depth_first_search.h"
#include "flexible_astar.h"
#include "four_connected_jps_locator.h"
#include "greedy_depth_first_search.h"
#include "gridmap.h"
#include "gridmap_expansion_policy.h"
#include "jps_expansion_policy.h"
#include "jps2_expansion_policy.h"
#include "jps2plus_expansion_policy.h"
#include "jps4c_expansion_policy.h"
#include "jpsplus_expansion_policy.h"
#include "ll_expansion_policy.h"
#include "manhattan_heuristic.h"
#include "octile_heuristic.h"
#include "scenario_manager.h"
#include "timer.h"
#include "labelled_gridmap.h"
#include "sipp_expansion_policy.h"
#include "vl_gridmap_expansion_policy.h"
#include "zero_heuristic.h"

#include "getopt.h"

#include <fstream>
#include <functional>
#include <iomanip>
#include <sstream>
#include <unordered_map>
#include <memory>



namespace py = pybind11;

// check computed solutions are optimal
int checkopt = 0;
// print debugging info during search
int verbose = 0;
// display program help on startup
int print_help = 0;


int add(int i, int j){
    return i + j;
}

//void demo(int argc, std::vector<std::string> argv){
//    // check computed solutions are optimal
//    int checkopt = 0;
//    // print debugging info during search
//    int verbose = 0;
//    // display program help on startup
//    int print_help = 0;
//
//    warthog::util::param valid_args[] =
//    {
//            {"alg",  required_argument, 0, 1},
//            {"scen",  required_argument, 0, 0},
//            {"map",  required_argument, 0, 1},
//            {"gen", required_argument, 0, 3},
//            {"help", no_argument, &print_help, 1},
//            {"checkopt",  no_argument, &checkopt, 1},
//            {"verbose",  no_argument, &verbose, 1},
//            {0,  0, 0, 0}
//    };
//
////    char ** argv_t;
////    for
//
//    warthog::util::cfg cfg;
//    cfg.parse_args(argc, const_cast<char **>(argv), "a:b:c:def", valid_args);
//
//    std::string sfile = cfg.get_param_value("scen");
//    std::string alg = cfg.get_param_value("alg");
//    std::string gen = cfg.get_param_value("gen");
//    std::string mapname = cfg.get_param_value("map");
//
//    std::cout << "sfile: " << sfile << std::endl;
//    std::cout << "alg: " << alg << std::endl;
//    std::cout << "gen: " << gen << std::endl;
//    std::cout << "mapname: " << mapname << std::endl;
//}

bool
check_optimality(warthog::solution& sol, warthog::experiment* exp)
{
    uint32_t precision = 2;
    double epsilon = (1.0 / (int)pow(10, precision)) / 2;
    double delta = fabs(sol.sum_of_edge_costs_ - exp->distance());

    if( fabs(delta - epsilon) > epsilon)
    {
        std::stringstream strpathlen;
        strpathlen << std::fixed << std::setprecision(exp->precision());
        strpathlen << sol.sum_of_edge_costs_;

        std::stringstream stroptlen;
        stroptlen << std::fixed << std::setprecision(exp->precision());
        stroptlen << exp->distance();

        std::cerr << std::setprecision(exp->precision());
        std::cerr << "optimality check failed!" << std::endl;
        std::cerr << std::endl;
        std::cerr << "optimal path length: "<<stroptlen.str()
                  <<" computed length: ";
        std::cerr << strpathlen.str()<<std::endl;
        std::cerr << "precision: " << precision << " epsilon: "<<epsilon<<std::endl;
        std::cerr<< "delta: "<< delta << std::endl;
        exit(1);
    }
    return true;
}

void
run_experiments(warthog::search* algo, std::string alg_name,
                warthog::scenario_manager& scenmgr, bool verbose, bool checkopt,
                std::ostream& out)
{
    std::cout
            << "id\talg\texpanded\ttouched\treopen\tsurplus\theapops"
            << "\tnanos\tpcost\tplen\tmap\n";
    for(unsigned int i=0; i < scenmgr.num_experiments(); i++)
    {
        warthog::experiment* exp = scenmgr.get_experiment(i);

        uint32_t startid = exp->starty() * exp->mapwidth() + exp->startx();
        uint32_t goalid = exp->goaly() * exp->mapwidth() + exp->goalx();
        warthog::problem_instance pi(startid, goalid, verbose);
        warthog::solution sol;

        algo->get_path(pi, sol);

        out
                << i<<"\t"
                << alg_name << "\t"
                << sol.nodes_expanded_ << "\t"
                << sol.nodes_touched_ << "\t"
                << sol.nodes_reopen_ << "\t"
                << sol.nodes_surplus_ << "\t"
                << sol.heap_ops_ << "\t"
                << sol.time_elapsed_nano_ << "\t"
                << sol.sum_of_edge_costs_ << "\t"
                << (sol.path_.size()-1) << "\t"
                << scenmgr.last_file_loaded()
                << std::endl;

        if(checkopt) { check_optimality(sol, exp); }
    }
}


void
run_jps(warthog::scenario_manager& scenmgr, std::string mapname, std::string alg_name)
{
    warthog::gridmap map(mapname.c_str());
    warthog::jps_expansion_policy expander(&map);
    warthog::octile_heuristic heuristic(map.width(), map.height());
    warthog::pqueue_min open;

    warthog::flexible_astar<
    warthog::octile_heuristic,
    warthog::jps_expansion_policy,
    warthog::pqueue_min>
            astar(&heuristic, &expander, &open);

    run_experiments(&astar, alg_name, scenmgr,
                    verbose, checkopt, std::cout);
    std::cerr << "done. total memory: "<< astar.mem() + scenmgr.mem() << "\n";
}

void demo(std::string sfile, std::string mapname, std::string alg){

    warthog::scenario_manager scenmgr;
    scenmgr.load_scenario(sfile.c_str());

    if(mapname == "")
    { mapname = scenmgr.get_experiment(0)->map().c_str(); }

    run_jps(scenmgr, mapname, alg);

}





PYBIND11_MODULE(warthog, m) {

    m.def("add", &add, "add 2 value");

    m.def("demo", &demo);

}



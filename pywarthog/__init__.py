

from pywarthog.search_module import expansion_policy
from pywarthog.jps_module import *
from pywarthog.util_module import *
from pywarthog.domains_module import *

from pywarthog.heuristics_module import *

from pywarthog.search_module import *


flexible_astar_class_obj = {}
flexible_astar_class_obj[flexible_astar_jpsplus] = {}
flexible_astar_class_obj[flexible_astar_jpsplus]["H"] = octile_heuristic.__name__
flexible_astar_class_obj[flexible_astar_jpsplus]["E"] = jpsplus_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_jpsplus]["Q"] = pqueue_min.__name__

flexible_astar_class_obj[flexible_astar_jps2plus] = {}
flexible_astar_class_obj[flexible_astar_jps2plus]["H"] = octile_heuristic.__name__
flexible_astar_class_obj[flexible_astar_jps2plus]["E"] = jps2plus_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_jps2plus]["Q"] = pqueue_min.__name__

flexible_astar_class_obj[flexible_astar_jps2] = {}
flexible_astar_class_obj[flexible_astar_jps2]["H"] = octile_heuristic.__name__
flexible_astar_class_obj[flexible_astar_jps2]["E"] = jps2_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_jps2]["Q"] = pqueue_min.__name__

flexible_astar_class_obj[flexible_astar_jps] = {}
flexible_astar_class_obj[flexible_astar_jps]["H"] = octile_heuristic.__name__
flexible_astar_class_obj[flexible_astar_jps]["E"] = jps_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_jps]["Q"] = pqueue_min.__name__

flexible_astar_class_obj[flexible_astar_jps4c] = {}
flexible_astar_class_obj[flexible_astar_jps4c]["H"] = manhattan_heuristic.__name__
flexible_astar_class_obj[flexible_astar_jps4c]["E"] = jps4c_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_jps4c]["Q"] = pqueue_min.__name__


flexible_astar_class_obj[flexible_astar_astar] = {}
flexible_astar_class_obj[flexible_astar_astar]["H"] = octile_heuristic.__name__
flexible_astar_class_obj[flexible_astar_astar]["E"] = gridmap_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_astar]["Q"] = pqueue_min.__name__

flexible_astar_class_obj[flexible_astar_astar4c] = {}
flexible_astar_class_obj[flexible_astar_astar4c]["H"] = manhattan_heuristic.__name__
flexible_astar_class_obj[flexible_astar_astar4c]["E"] = gridmap_expansion_policy.__name__
flexible_astar_class_obj[flexible_astar_astar4c]["Q"] = pqueue_min.__name__


def init_flexible_astar(H, E, Q, heuristic, expander, queue):

    for key, value in flexible_astar_class_obj.items():
        if (value["H"] == H.__name__) and (value["E"] == E.__name__) and (value["Q"] == Q.__name__):
            return key(heuristic, expander, queue)

    return None
